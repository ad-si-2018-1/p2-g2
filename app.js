var express = require('express');  // módulo express
var app = express();		   // objeto express
var server = require('http').Server(app);
var socketio = require('socket.io')(server);

var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;
var servidoratual;

function proxy(id, servidor, nick, canal, socket) {

	var ws = socket;

	var irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal],});

	
	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	   
	    ws.emit(	"message", {"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );

	});
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	irc_client.addListener('mode', function(message) {
	    console.log('mode: ', message);
	});
	irc_client.addListener('motd', function(motd) {
		console.log('motd: ', motd);
		
		ws.emit(	"message", {
		"msg":motd} );
	});
	irc_client.addListener('nick', function(oldnick, newnick, channels, message) {
		
		ws.emit(	"message", {
		"msg":oldnick + " is now known as " + newnick} );
		
		irc_client.send("NAMES", channels[0])
	});
	irc_client.addListener('whois', function(info) {
		console.log('mode: ', info);
		servidoratual = info.server;
		ws.emit(	"message", {
			"msg":JSON.stringify(info)} );
			
	});
	irc_client.addListener('names', function(channel, nicks) {
		var lista = channel+JSON.stringify(nicks);
		console.log('listagem de nicks do canal: '+lista);
		ws.emit("names", {
			"msg":lista} );
	});
	irc_client.addListener('raw', function(time) {
		

		//console.log(time.command);
		if (time.command === "rpl_time"){
		console.log(time);
		ws.emit(	"message", {
			"msg":time.args[2]} );
		}
	});
	irc_client.addListener('quit', function(nick, reason, channels, message) {

		ws.emit(	"message", {
			"msg":nick + " has left " + channels[0]} );
			
		irc_client.send("NAMES", channels[0])
		
	});
	irc_client.addListener('join', function(channel, nick, message) {
		
				ws.emit(	"message", {
					"msg":nick + " has join to " + channel} );
					
				irc_client.send("NAMES", channel)
	});
	
	proxies[id] = { "ws":socket, "irc_client":irc_client  };

	return proxies[id];
}

app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
	proxy_id++;

	socketio.on("connection", function (socket) {
		
		var p =	proxy(	proxy_id,
				req.cookies.servidor,
				req.cookies.nick, 
				req.cookies.canal,
				socket);
		
				socket.on("message", function (message) {
			
					console.log("mensagem:"+message);
					
					var texto = message;
					
					if (texto.substring(0,5) == "/calc" || texto.substring(0,5) == "/CALC"){
 
						var result = eval(texto.substring(5,texto.length));
						socket.emit(	"message", {
						"msg":result} );

					} else if(texto.substring(0,5) == "/NICK" || texto.substring(0,5) == "/nick"){

						var novonick = texto.substring(6, texto.length);
						var semespaco = novonick.split(" ");
						proxies[proxy_id].irc_client.send("NICK", semespaco[0]);

					} else if(texto.substring(0,5) == "/MOTD" || texto.substring(0,5) == "/motd"){

						proxies[proxy_id].irc_client.send("MOTD");

					} else if(texto.substring(0,5) == "/JOIN" || texto.substring(0,5) == "/join"){

						proxies[proxy_id].irc_client.join(req.cookies.canal);

					} else if(texto.substring(0,6) == "/WHOIS" || texto.substring(0,6) == "/whois"){

						proxies[proxy_id].irc_client.send("WHOIS", req.cookies.nick);

					} else if(texto.substring(0,6) == "/NAMES" || texto.substring(0,6) == "/names"){

						proxies[proxy_id].irc_client.send("NAMES", req.cookies.canal);

					} else if(texto.substring(0,5) == "/TIME" || texto.substring(0,5) == "/time"){
						
						proxies[proxy_id].irc_client.send("TIME", servidoratual);

					} else {

						proxies[proxy_id].irc_client.say(req.cookies.canal, message);
					}

		})
	});
	
	res.cookie('id', proxy_id);
  res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  }
});

app.post('/gravar_mensagem', function (req, res) {
  // proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  irc_client.say(req.cookies.canal, irc_client.opt.channels[0], req.body.msg );
  res.end();
});

app.get('/mode/:usuario/:args', function (req, res){
	
	var usuario = req.params.usuario;
  var args = req.params.args;
  //var retorno = '{"usuario":'+usuario+','+
  //		  '"args":"'+args+'}';
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", usuario, args);
	res.send(retorno);
	
});
app.get('/mode/', function (req, res){
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", req.cookies.nick);
  res.send(retorno);
});


app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

server.listen(3000, function () {				
  console.log('Example app listening on port 3000!');	
});
